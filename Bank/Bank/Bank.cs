﻿using Bank.Operacje;
using System.Collections.Generic;

namespace Bank
{
    public class Bank : IBank
    {
        private int id;
        private static int nextId = 0;
        private List<PrzelewMiedzybankowy> listaPrzelewowMiedzybankowych;
        public List<PrzelewMiedzybankowy> ListaPrzelewowMiedzyBankowych
        {
            get
            {
                return listaPrzelewowMiedzybankowych;
            }

        }


        private List<IKlient> klienci;
        public List<IKlient> Klienci
        {
            get
            {
                return klienci;
            }
        }
        private List<IProduktBankowy> produkty;
        public List<IProduktBankowy> Produkty
        {
            get
            {
                return produkty;
            }
        }

        public Bank()
        {
            id = nextId;
            nextId++;
            produkty = new List<IProduktBankowy>();
            klienci = new List<IKlient>();
            listaPrzelewowMiedzybankowych = new List<PrzelewMiedzybankowy>();
        }

        public bool DodajKlienta(IKlient klient)
        {
            if (klient == null)
            {
                return false;
            }
            klienci.Add(klient);
            return true;
        }

        public IKonto UtworzKonto(IKlient klient)
        {
            if (klienci.Exists((k) => k == klient))
            {
                Konto konto = new Konto(klient, this, new OdsetkiKonta());
                produkty.Add(konto);
                return konto;
            }
            return null;
        }

        public bool ZweryfikujKredyt(IKonto konto)
        {
            if (produkty.Exists((p) => p == konto))
            {
                return true;
            }
            return false;
        }

        public void DodajProdukt(IProduktBankowy produkt)
        {
            produkty.Add(produkt);
        }

        public bool ZmienMechanizmOdsetek(IKonto konto, IOdsetkiKonta noweOdsetki)
        {
            if (produkty.Exists(p => p == konto))
            {
                konto.ZmienMechanizmOdsetkowy(noweOdsetki, "");
                return true;
            }
            return false;
        }


        public IKonto ZwrocKonto(string nrRachunku)
        {
            foreach (ProduktBankowy produkt in produkty)
            {
                if (produkt is IKonto)
                {
                    IKonto konto = produkt as IKonto;
                    if (konto.NrRachunku == nrRachunku)
                    {
                        return konto;
                    }
                }
            }
            return null;
        }

        public void WyslijPrzelewyMiedzybankowe(List<PrzelewMiedzybankowy> listaPrzelewowMiedzybankowych)
        {
            Mediator.GetMediator.OdbierzPrzelewy(listaPrzelewowMiedzybankowych);
        }

        public void DodajPrzelewMiedzybankowy(PrzelewMiedzybankowy przelewMiedzybankowy)
        {
            listaPrzelewowMiedzybankowych.Add(przelewMiedzybankowy);
        }

        public List<IProduktBankowy> ZrobRaport(IRaportSelekcja raport)
        {
            List<IProduktBankowy> wynik = new List<IProduktBankowy>();
            foreach (IProduktBankowy produktBankowy in produkty)
            {
                wynik.Add(produktBankowy.Akceptuj(raport));
            }
            return wynik;
        }

        public double ZrobRaport(IRaportAgregacja raport)
        {
            double wynik = 0;
            foreach (IProduktBankowy produktBankowy in produkty)
            {
                wynik += produktBankowy.Akceptuj(raport);
            }
            return wynik;
        }
    }
}
