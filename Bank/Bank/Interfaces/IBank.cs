﻿using Bank.Operacje;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public interface IBank
    {
        bool DodajKlienta(IKlient klient);

        IKonto UtworzKonto(IKlient klient);

        bool ZweryfikujKredyt(IKonto konto);

        void DodajProdukt(IProduktBankowy produkt);

        bool ZmienMechanizmOdsetek(IKonto konto, IOdsetkiKonta noweOdsetki);

        IKonto ZwrocKonto(string nrRachunku);

        void WyslijPrzelewyMiedzybankowe(List<PrzelewMiedzybankowy> listaPrzelewowMiedzybankowych);

        void DodajPrzelewMiedzybankowy(PrzelewMiedzybankowy przelewMiedzyBankowy);

        List<IProduktBankowy> ZrobRaport(IRaportSelekcja raport);

        double ZrobRaport(IRaportAgregacja raport);
    }
}
