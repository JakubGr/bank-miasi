﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public interface IKlient
    {
        bool UtworzKonto(IBank bank);

        bool WykonajPrzelew(string nrRachunkuNadawcy, string nrRachunkuOdbiorcy, double kwotaPrzelewu, string opis);

        bool PobierzPieniadze(string nrRachunku, double kwota, string opis);

        bool WplacPieniadze(string nrRachunku, double kwota, string opis);

        bool ZalozLokate(string nrRachunku, double srodkiNaLokate, DateTime terminRozpoczeciaLokaty, string opis);

        bool ZerwijLokate(string nrRachunku, ILokata lokata, string opis);

        bool WezKredyt(string nrRachunku, double wysokoscKredytu, string opis);

        bool SplacRateKredytu(string nrRachunku, IKredyt kredyt, string opis);
    }
}
