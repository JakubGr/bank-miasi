﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public interface IKonto: IProduktBankowy
    {
        string NrRachunku { get; }

        double Saldo { get; }

        List<ILokata> Lokaty { get; }

        List<IKredyt> Kredyty { get; }

        bool MozliwePobranie(double kwota);

        bool PobierzPieniadzeZRachunku(double kwota);

        bool WplacPieniadzeNaRachunek(double kwota);

        void WykonajOperacje(Operacja operacja);

        bool Przelew(string nrRachunkuOdbiorcy, double kwotaPrzelewu, string opis);

        bool Wyplata(double kwota, string opis);

        bool Wplata(double kwota, string opis);

        bool NaliczOdsetki(string opis);

        bool ZmienMechanizmOdsetkowy(IOdsetkiKonta noweOdsetki, string opis);

        bool ZalozLokate(double srodkiNaLokate, DateTime terminRozpoczeciaLokaty, string opis);

        bool ZerwijLokate(ILokata lokata, string opis);

        bool ZaciagnijKredyt(double wysokoscKredytu, string opis);

        bool SplacRateKredytu(IKredyt kredyt, string opis);

        bool ZweryfikujKredyt();

        void DodajKredyt(IKredyt kredyt);

        void DodajLokate(ILokata lokata);

        bool Zwrot(double kwota, string opis);
    }
}
