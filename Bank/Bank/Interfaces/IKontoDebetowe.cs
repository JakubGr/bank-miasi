﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public interface IKontoDebetowe : IKonto
    {
        IKonto Konto { get; }

        double WysokoscDebetu { get; }
    }
}
