﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public interface IKredyt : IProduktBankowy
    {
        double RataKredytu { get; }
        double WysokoscKredytu { get; }
        DateTime DataZakonczenia { get; }

        bool WezKredyt(double wysokoscKredytu, IKonto konto);

        void SplacRate();
    }
}
