﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public interface ILokata : IProduktBankowy
    {
        void UtworzLokate(double srodkiNaLokacie, DateTime poczatekLokaty);

        double WysokoscLokaty { get; }
        double ZakonczLokate();
    }
}
