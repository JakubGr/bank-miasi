﻿using System.Collections.Generic;

namespace Bank
{
    public interface IProduktBankowy
    {
        List<Operacja> HistoriaOperacji { get; }

        IProduktBankowy Akceptuj(IRaportSelekcja raport);

        double Akceptuj(IRaportAgregacja raport); 
    }
}
