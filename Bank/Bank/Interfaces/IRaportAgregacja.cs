﻿namespace Bank
{
    public interface IRaportAgregacja
    {
        double Wybierz(IKonto konto);

        double Wybierz(IKredyt kredyt);

        double Wybierz(ILokata lokata);

        double Wybierz(IKontoDebetowe kontoDebetowe);
    }
}
