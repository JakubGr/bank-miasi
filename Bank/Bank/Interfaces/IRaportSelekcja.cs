﻿namespace Bank
{
    public interface IRaportSelekcja
    {
        IProduktBankowy Wybierz(IKonto konto);

        IProduktBankowy Wybierz(IKredyt kredyt);

        IProduktBankowy Wybierz(ILokata lokata);

        IProduktBankowy Wybierz(IKontoDebetowe kontoDebetowe);
    }
}
