﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class Klient : IKlient
    {
        private static int nextId = 0;
        int id;
        string imie, nazwisko, adres, email, dokumentTozsamosci, telefon;

        public List<IKonto> konta;

        public Klient(string imie, string nazwisko, string adres, string email, string dokumentTozsamosci, string telefon)
        {
            this.id = nextId;
            nextId++;

            this.imie = imie;
            this.nazwisko = nazwisko;
            this.adres = adres;
            this.email = email;
            this.dokumentTozsamosci = dokumentTozsamosci;
            this.telefon = telefon;

            this.konta = new List<IKonto>();
        }

        public bool UtworzKonto(IBank bank)
        {
            IKonto noweKonto = bank.UtworzKonto(this);
            if(noweKonto == null)
            {
                return false;
            }
            konta.Add(noweKonto);
            return true;
        }

        public bool WykonajPrzelew(string nrRachunkuNadawcy, string nrRachunkuOdbiorcy, double kwotaPrzelewu, string opis)
        {
            IKonto kontoNadawcy = konta.Find((IKonto k) => k.NrRachunku == nrRachunkuNadawcy);
            if (kontoNadawcy != null)
            {
                return kontoNadawcy.Przelew(nrRachunkuOdbiorcy, kwotaPrzelewu, opis);
            }
            return false;
        }
        
        public bool PobierzPieniadze(string nrRachunku, double kwota, string opis)
        {
            IKonto nadawca = konta.Find((IKonto k) => k.NrRachunku == nrRachunku);
            if (nadawca != null)
            {
                return nadawca.Wyplata(kwota, opis);
            }
            return false;
        }

        public bool WplacPieniadze(string nrRachunku, double kwota, string opis)
        {
            IKonto konto = konta.Find((IKonto k) => k.NrRachunku == nrRachunku);
            if (konto != null)
            {
                return konto.Wplata(kwota, opis);
            }
            return false;
        }

        public bool ZalozLokate(string nrRachunku, double srodkiNaLokate, DateTime terminRozpoczeciaLokaty, string opis)
        {
            IKonto nadawca = konta.Find((IKonto k) => k.NrRachunku == nrRachunku);
            if (nadawca != null)
            {
                return nadawca.ZalozLokate(srodkiNaLokate, terminRozpoczeciaLokaty, opis);
            }
            return false;
        }

        public bool ZerwijLokate(string nrRachunku, ILokata lokata, string opis)
        {
            IKonto nadawca = konta.Find((IKonto k) => k.NrRachunku == nrRachunku);
            if (nadawca != null)
            {
                return nadawca.ZerwijLokate(lokata, opis);
            }
            return false;
        }

        public bool WezKredyt(string nrRachunku, double wysokoscKredytu, string opis)
        {
            IKonto nadawca = konta.Find((IKonto k) => k.NrRachunku == nrRachunku);
            if (nadawca != null)
            {
                return nadawca.ZaciagnijKredyt(wysokoscKredytu, opis);
            }
            return false;
        }

        public bool SplacRateKredytu(string nrRachunku, IKredyt kredyt, string opis)
        {
            IKonto nadawca = konta.Find((IKonto k) => k.NrRachunku == nrRachunku);
            if (nadawca != null)
            {
                return nadawca.SplacRateKredytu(kredyt, opis);
            }
            return false;
        }
    }
}
