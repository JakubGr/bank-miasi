﻿using Bank.Operacje;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class Konto : ProduktBankowy, IKonto
    {
        private string nrRachunku;
        public string NrRachunku
        {
            get
            {
                return nrRachunku;
            }
        }
        private double saldo;
        public double Saldo
        {
            get
            {
                return saldo;
            }
        }

        private IOdsetkiKonta oprocentowanie;
        private DateTime dataZalozenia;
        private IKlient wlasciciel;
        public IKlient Wlasciciel
        {
            get
            {
                return wlasciciel;
            }
        }
        private IBank bank;


        private List<ILokata> lokaty;
        public List<ILokata> Lokaty
        {
            get
            {
                return lokaty;
            }
        }
        private List<IKredyt> kredyty;
        public List<IKredyt> Kredyty
        {
            get
            {
                return kredyty;
            }
        }

        public Konto(IKlient klient, IBank bank, IOdsetkiKonta oprocentowanie)
            : base()
        {
            this.saldo = 0.0;
            this.nrRachunku = "";
            this.wlasciciel = klient;
            this.bank = bank;
            dataZalozenia = DateTime.Today;

            this.oprocentowanie = oprocentowanie;

            lokaty = new List<ILokata>();
            kredyty = new List<IKredyt>();
        }

        public bool MozliwePobranie(double kwota)
        {
            if (kwota >= 0.0 && kwota < saldo)
            {
                return true;
            }
            return false;
        }

        public bool PobierzPieniadzeZRachunku(double kwota)
        {
            if (!MozliwePobranie(kwota))
            {
                return false;
            }
            else
            {
                saldo -= kwota;
            }
            return true;
        }

        public bool WplacPieniadzeNaRachunek(double kwota)
        {
            if (kwota > 0.0)
            {
                saldo += kwota;
                return true;
            }
            return false;
        }

        public void WykonajOperacje(Operacja operacja)
        {
            operacja.WykonajOperacje();
            historiaOperacji.Add(operacja);
        }

        public bool Przelew(string nrRachunkuOdbiorcy, double kwotaPrzelewu, string opis)
        {
            IKonto odbiorca = bank.ZwrocKonto(nrRachunkuOdbiorcy);
            if (odbiorca != null)
            {
                Operacja przelew = new Przelew(this, odbiorca, kwotaPrzelewu, opis);
                WykonajOperacje(przelew);
                return (bool)przelew.Wykonana;
            }
            else
            {
                Operacja przelew = new PrzelewMiedzybankowy(this, bank, nrRachunkuOdbiorcy, kwotaPrzelewu, opis);
                WykonajOperacje(przelew);
                return (bool)przelew.Wykonana;
            }
        }

        public bool Wyplata(double kwota, string opis)
        {
            Operacja wyplata = new Wyplata(this, kwota, opis);
            WykonajOperacje(wyplata);
            return (bool)wyplata.Wykonana;
        }

        public bool Wplata(double kwota, string opis)
        {
            Operacja wplata = new Wplata(this, kwota, opis);
            WykonajOperacje(wplata);
            return (bool)wplata.Wykonana;
        }

        public bool Zwrot(double kwota, string opis)
        {
            Operacja zwrot = new Zwrot(this, kwota, opis);
            WykonajOperacje(zwrot);
            return (bool)zwrot.Wykonana;
        }

        public bool NaliczOdsetki(string opis)
        {
            Operacja naliczenieOdsetek = new NaliczenieOdsetek(this, oprocentowanie.ObliczOdsetki(this), opis);
            WykonajOperacje(naliczenieOdsetek);
            return (bool)naliczenieOdsetek.Wykonana;
        }

        public bool ZmienMechanizmOdsetkowy(IOdsetkiKonta noweOdsetki, string opis)
        {
            Operacja zmiana = new ZmianaMechanizmuOdsetkowego(this, noweOdsetki, opis);
            WykonajOperacje(zmiana);
            return (bool)zmiana.Wykonana;
        }

        public bool ZalozLokate(double srodkiNaLokate, DateTime terminRozpoczeciaLokaty, string opis)
        {
            Operacja zalozenieLokaty = new ZalozenieLokaty(this, srodkiNaLokate, terminRozpoczeciaLokaty, opis);
            WykonajOperacje(zalozenieLokaty);
            return (bool)zalozenieLokaty.Wykonana;
        }

        public bool ZerwijLokate(ILokata lokata, string opis)
        {
            if (lokaty.Exists((l) => l == lokata))
            {
                Operacja zerwanieLokaty = new ZakonczenieLokaty(this, lokata, true, opis);
                WykonajOperacje(zerwanieLokaty);
                lokaty.Remove(lokata);
                return (bool)zerwanieLokaty.Wykonana;
            }
            return false;
        }

        public bool ZaciagnijKredyt(double wysokoscKredytu, string opis)
        {
            Operacja zaciagniecieKredytu = new ZaciagniecieKredytu(this, wysokoscKredytu, opis);
            WykonajOperacje(zaciagniecieKredytu);
            return (bool)zaciagniecieKredytu.Wykonana;
        }

        public bool SplacRateKredytu(IKredyt kredyt, string opis)
        {
            Operacja splataRatyKredytu = new SplataRatyKredytu(this, kredyt, opis);
            WykonajOperacje(splataRatyKredytu);
            return (bool)splataRatyKredytu.Wykonana;
        }

        public bool ZweryfikujKredyt()
        {
            return bank.ZweryfikujKredyt(this);
        }

        public void DodajKredyt(IKredyt kredyt)
        {
            kredyty.Add(kredyt);
        }

        public void DodajLokate(ILokata lokata)
        {
            lokaty.Add(lokata);
        }

        public override IProduktBankowy Akceptuj(IRaportSelekcja raport)
        {
            return raport.Wybierz(this);
        }

        public override double Akceptuj(IRaportAgregacja raport)
        {
            return raport.Wybierz(this);
        }
    }
}
