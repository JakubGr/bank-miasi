﻿using Bank.Operacje;
using System;
using System.Collections.Generic;

namespace Bank
{
    public class KontoDebetowe : IKontoDebetowe
    {
        private IKonto konto;

        public IKonto Konto
        {
            get
            {
                return konto;
            }
        }

        public string NrRachunku
        {
            get
            {
                return konto.NrRachunku;
            }
        }

        public double Saldo
        {
            get
            {
                return konto.Saldo - wysokoscDebetu;
            }
        }

        public List<Operacja> HistoriaOperacji
        {
            get
            {
                return konto.HistoriaOperacji;
            }
        }

        public List<ILokata> Lokaty
        {
            get
            {
                return konto.Lokaty;
            }
        }

        public List<IKredyt> Kredyty
        {
            get
            {
                return konto.Kredyty;
            }
        }

        private double wysokoscDebetu;

        public double WysokoscDebetu
        {
            get
            {
                return wysokoscDebetu;
            }
        }

        private double maxDebet;

        public KontoDebetowe(IKonto konto, double maxDebet)
        {
            this.konto = konto;
            this.maxDebet = maxDebet;
            wysokoscDebetu = 0.0;
        }

        public void DodajKredyt(IKredyt kredyt)
        {
            konto.DodajKredyt(kredyt);
        }

        public void DodajLokate(ILokata lokata)
        {
            konto.DodajLokate(lokata);
        }

        public bool MozliwePobranie(double kwota)
        {
            if (konto.MozliwePobranie(kwota) || (kwota >= 0 && Saldo + maxDebet >= kwota))
            {
                return true;
            }
            return false;
        }

        public bool NaliczOdsetki(string opis)
        {
            return konto.NaliczOdsetki(opis);
        }

        public bool PobierzPieniadzeZRachunku(double kwota)
        {
            if (MozliwePobranie(kwota))
            {
                if (!konto.MozliwePobranie(kwota))
                {
                    wysokoscDebetu += kwota - konto.Saldo;
                    kwota = konto.Saldo;
                    Operacja stworzenieDebetu = new StworzenieDebetu(this, "");
                    WykonajOperacje(stworzenieDebetu);
                }
                return konto.PobierzPieniadzeZRachunku(kwota);
            }
            return false;
        }

        public void WykonajOperacje(Operacja operacja)
        {
            konto.WykonajOperacje(operacja);
        }

        public bool Przelew(string nrRachunkuOdbiorcy, double kwotaPrzelewu, string opis)
        {
            IKonto odbiorca = System.ZwrocKonto(nrRachunkuOdbiorcy);
            if (odbiorca != null)
            {
                Operacja przelew = new Przelew(this, odbiorca, kwotaPrzelewu, opis);
                WykonajOperacje(przelew);
                return (bool)przelew.Wykonana;
            }
            return false;
        }

        public bool SplacRateKredytu(IKredyt kredyt, string opis)
        {
            Operacja splataRatyKredytu = new SplataRatyKredytu(this, kredyt, opis);
            WykonajOperacje(splataRatyKredytu);
            return (bool)splataRatyKredytu.Wykonana;
        }

        public bool WplacPieniadzeNaRachunek(double kwota)
        {
            if (kwota > 0.0)
            {
                if (wysokoscDebetu > 0.0)
                {
                    double splataDebetu = Math.Min(wysokoscDebetu, kwota);
                    wysokoscDebetu -= splataDebetu;
                    kwota -= splataDebetu;
                }
                return konto.WplacPieniadzeNaRachunek(kwota); ;
            }
            return false;
        }

        public bool Wplata(double kwota, string opis)
        {
            Operacja wplata = new Wplata(this, kwota, opis);
            WykonajOperacje(wplata);
            return (bool)wplata.Wykonana;
        }

        public bool Wyplata(double kwota, string opis)
        {
            Operacja wyplata = new Wyplata(this, kwota, opis);
            WykonajOperacje(wyplata);
            return (bool)wyplata.Wykonana;
        }

        public bool ZaciagnijKredyt(double wysokoscKredytu, string opis)
        {
            Operacja zaciagniecieKredytu = new ZaciagniecieKredytu(this, wysokoscKredytu, opis);
            WykonajOperacje(zaciagniecieKredytu);
            return (bool)zaciagniecieKredytu.Wykonana;
        }

        public bool ZalozLokate(double srodkiNaLokate, DateTime terminRozpoczeciaLokaty, string opis)
        {
            Operacja zalozenieLokaty = new ZalozenieLokaty(this, srodkiNaLokate, terminRozpoczeciaLokaty, opis);
            WykonajOperacje(zalozenieLokaty);
            return (bool)zalozenieLokaty.Wykonana;
        }

        public bool ZerwijLokate(ILokata lokata, string opis)
        {
            if (konto.Lokaty.Exists((l) => l == lokata))
            {
                Operacja zerwanieLokaty = new ZakonczenieLokaty(this, lokata, true, opis);
                WykonajOperacje(zerwanieLokaty);
                konto.Lokaty.Remove(lokata);
                return (bool)zerwanieLokaty.Wykonana;
            }
            return false;
        }

        public bool ZmienMechanizmOdsetkowy(IOdsetkiKonta noweOdsetki, string opis)
        {
            return konto.ZmienMechanizmOdsetkowy(noweOdsetki, opis);
        }

        public bool ZweryfikujKredyt()
        {
            return konto.ZweryfikujKredyt();
        }

        public IProduktBankowy Akceptuj(IRaportSelekcja raport)
        {
            return raport.Wybierz(this);
        }

        public double Akceptuj(IRaportAgregacja raport)
        {
            return raport.Wybierz(this);
        }

        public bool Zwrot(double kwota, string opis)
        {
            Operacja zwrot = new Zwrot(this, kwota, opis);
            WykonajOperacje(zwrot);
            return (bool)zwrot.Wykonana;
        }
    }
}
