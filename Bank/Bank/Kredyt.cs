﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class Kredyt : ProduktBankowy, IKredyt
    {
        private double wysokoscKredytu;
        public double WysokoscKredytu
        {
            get
            {
                return wysokoscKredytu;
            }
        }
        private int dlugoscKredytuWMiesiacach;
        private DateTime poczatekKredytu;
        private IOdsetkiKredytu oprocentowanie;
        private double rataKredytu;
        private double pozostaleObciazenie;

        public double RataKredytu
        {
            get
            {
                return rataKredytu;
            }
        }

        private DateTime dataZakonczenia;
        public DateTime DataZakonczenia {
            get
            {
                return dataZakonczenia;
            }
        }

        public Kredyt(int dlugoscKredytuWMiesiacach, IOdsetkiKredytu oprocentowanie) : base()
        {
            this.dlugoscKredytuWMiesiacach = dlugoscKredytuWMiesiacach;
            this.oprocentowanie = oprocentowanie;
            dataZakonczenia = poczatekKredytu.AddMonths(dlugoscKredytuWMiesiacach);
        }

        public bool WezKredyt(double wysokoscKredytu, IKonto konto)
        {
            if (konto.ZweryfikujKredyt())
            {
                this.wysokoscKredytu = wysokoscKredytu;
                poczatekKredytu = DateTime.Now;
                rataKredytu = (wysokoscKredytu + oprocentowanie.ObliczOdsetki(this)) / (dlugoscKredytuWMiesiacach);
                pozostaleObciazenie = wysokoscKredytu;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void SplacRate()
        {
            pozostaleObciazenie -= rataKredytu;
        }

        public override IProduktBankowy Akceptuj(IRaportSelekcja raport)
        {
            return raport.Wybierz(this);
        }

        public override double Akceptuj(IRaportAgregacja raport)
        {
            return raport.Wybierz(this);
        }
    }
}
