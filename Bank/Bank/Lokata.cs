﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class Lokata : ProduktBankowy, ILokata
    {
        private double srodkiNaLokacie;
        public double WysokoscLokaty
        {
            get
            {
                return srodkiNaLokacie;
            }
        }
        private int dlugoscLokatyWMiesiacach;
        private DateTime poczatekLokaty;
        private IOdsetkiLokaty oprocentowanie;

        public Lokata(int dlugoscLokatyWMiesiacach, IOdsetkiLokaty oprocentowanie)
        {
            this.dlugoscLokatyWMiesiacach = dlugoscLokatyWMiesiacach;
            this.oprocentowanie = oprocentowanie;
        }

        public void UtworzLokate(double srodkiNaLokacie, DateTime poczatekLokaty)
        {
            this.srodkiNaLokacie = srodkiNaLokacie;
            this.poczatekLokaty = poczatekLokaty;
        }

        public double ZakonczLokate()
        {
            double zwrot = 0.0;

            DateTime now = DateTime.Now;
            if ((now.Year - poczatekLokaty.Year) * 12 + now.Month - poczatekLokaty.Month < dlugoscLokatyWMiesiacach)
            {
                zwrot = srodkiNaLokacie;
            }
            else
            {
                zwrot = srodkiNaLokacie + oprocentowanie.ObliczOdsetki(this);
            }
            srodkiNaLokacie = 0.0;

            return zwrot;
        }

        public override IProduktBankowy Akceptuj(IRaportSelekcja raport)
        {
            return raport.Wybierz(this);
        }

        public override double Akceptuj(IRaportAgregacja raport)
        {
            return raport.Wybierz(this);
        }
    }
}
