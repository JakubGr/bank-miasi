﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Operacje
{
    public class Mediator
    {
        private static Mediator mediator;
        private List<IBank> listaBankow;

        public List<IBank> ListaBankow
        {
            get
            {
                return listaBankow;
            }
        }

        public static Mediator GetMediator
        {
            get
            {
                if (mediator == null)
                {
                    mediator = new Mediator();
                }
                return mediator;
            }
        }

        private Mediator()
        {
            listaBankow = new List<IBank>();
        }

        public void OdbierzPrzelewy(List<PrzelewMiedzybankowy> listaPrzelewowMiedzyBankowych)
        {
            foreach (PrzelewMiedzybankowy przelewMiedzyBankowy in listaPrzelewowMiedzyBankowych)
            {

                WyslijPrzelew(przelewMiedzyBankowy);

            }
        }

        public void WyslijPrzelew(PrzelewMiedzybankowy przelewMiedzybankowy)
        {
            bool wynik = false;

            foreach (IBank bank in ListaBankow)
            {
                IKonto konto = bank.ZwrocKonto(przelewMiedzybankowy.NrRachunkuOdbiorcy);

                if (konto != null)
                {

                    wynik = konto.Wplata(przelewMiedzybankowy.KwotaPrzelewu, "");
                    break;
                 
                }
            }

            if (!wynik)
            {
                przelewMiedzybankowy.Konto.Zwrot(przelewMiedzybankowy.KwotaPrzelewu, "");
            }

        }

    }
}
