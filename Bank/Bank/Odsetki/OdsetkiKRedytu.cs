﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class OdsetkiKredytu : IOdsetkiKredytu
    {
        public double ObliczOdsetki(IKredyt ikredyt)
        {
            return 0.05 * ikredyt.WysokoscKredytu;
        }
    }
}
