﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class OdsetkiKonta : IOdsetkiKonta
    {

        public double ObliczOdsetki(IKonto ikonto)
        {
            return 0.001 * ikonto.Saldo;
        }
    }
}
