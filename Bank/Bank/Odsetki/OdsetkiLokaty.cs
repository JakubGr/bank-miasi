﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class OdsetkiLokaty : IOdsetkiLokaty
    {
        public double ObliczOdsetki(ILokata ilokata)
        {
            return 0.02 * ilokata.WysokoscLokaty;
        }
    }
}
