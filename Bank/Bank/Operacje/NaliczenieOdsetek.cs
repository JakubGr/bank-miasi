﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    class NaliczenieOdsetek : Operacja
    {
        private Konto konto;
        private double wysokoscOdsetek;

        internal NaliczenieOdsetek(Konto konto, double wysokoscOdsetek, string opis) : base(opis)
        {
            this.konto = konto;
            this.wysokoscOdsetek = wysokoscOdsetek;
        }

        public override void WykonajOperacje()
        {
            base.WykonajOperacje();
            konto.WplacPieniadzeNaRachunek(wysokoscOdsetek);
        }
    }
}
