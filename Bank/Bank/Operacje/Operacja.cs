﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public abstract class Operacja
    {
        private int id;
        private static int nextId = 0;
        protected bool? wykonana;
        public bool? Wykonana
        {
            get
            {
                return wykonana;
            }
        }

        protected DateTime dataRealizacji;
        protected string opis;

        public Operacja(string opis)
        {
            wykonana = null;
            id = nextId;
            nextId++;

            this.opis = opis;
        }

        public virtual void WykonajOperacje()
        {
            dataRealizacji = DateTime.Now;
            wykonana = true;
        }
    }
}
