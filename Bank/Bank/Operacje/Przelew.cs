﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class Przelew : Operacja
    {
        private IKonto nadawca;
        private IKonto odbiorca;
        private double kwotaPrzelewu;

        public Przelew(IKonto nadawca, IKonto odbiorca, double kwotaPrzelewu, string opis) : base(opis)
        {
            this.nadawca = nadawca;
            this.odbiorca = odbiorca;
            this.kwotaPrzelewu = kwotaPrzelewu;
        }

        public override void WykonajOperacje()
        {
            WykonajPrzelew();
        }

        public void WykonajPrzelew()
        {
            dataRealizacji = DateTime.Now;
            if (nadawca.MozliwePobranie(kwotaPrzelewu))
            {
                wykonana = (nadawca.PobierzPieniadzeZRachunku(kwotaPrzelewu) && odbiorca.WplacPieniadzeNaRachunek(kwotaPrzelewu));
            }
            else
            {
                wykonana = false;
            }
        }
    }
}
