﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Operacje
{
    public class PrzelewMiedzybankowy :Operacja
    {
        private IKonto konto;
        public IKonto Konto
        {
            get
            {
            return konto;
            }
        }
        private IBank bank;
        public IBank Bank
        {
            get
            {
                return bank;
            }
        }
        private string nrRachunkuOdbiorcy;
        public string NrRachunkuOdbiorcy
        {
            get
            {
                return nrRachunkuOdbiorcy;
            }
        }
        private double kwotaPrzelewu;
        public double KwotaPrzelewu
        {
            get
            {
                return kwotaPrzelewu;
            }
        }

        public PrzelewMiedzybankowy(IKonto konto, IBank bank, string nrRachunkuOdbiorcy, double kwotaPrzelewu, string opis) : base(opis)
        {
            this.konto = konto;
            this.bank = bank;
            this.nrRachunkuOdbiorcy = nrRachunkuOdbiorcy;
            this.kwotaPrzelewu = kwotaPrzelewu;
            this.opis = opis;
        }

        public override void WykonajOperacje()
        {
            WykonajPrzelew();
        }

        private void WykonajPrzelew()
        {
            dataRealizacji = DateTime.Now;


            bank.DodajPrzelewMiedzybankowy(this);
        }


    }
}
