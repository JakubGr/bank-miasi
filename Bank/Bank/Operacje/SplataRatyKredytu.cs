﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class SplataRatyKredytu : Operacja
    {
        private IKonto kontoPowiazaneZKredytem;
        private IKredyt kredyt;
        private double wysokoscRaty;

        public SplataRatyKredytu(IKonto kontoPowiazaneZKredytem, IKredyt kredyt, string opis) : base(opis)
        {
            this.kontoPowiazaneZKredytem = kontoPowiazaneZKredytem;
            this.kredyt = kredyt;
            this.wysokoscRaty = this.kredyt.RataKredytu;
        }

        public override void WykonajOperacje()
        {
            SplacRate();
        }

        private void SplacRate()
        {
            if (kontoPowiazaneZKredytem.MozliwePobranie(wysokoscRaty) && kontoPowiazaneZKredytem.PobierzPieniadzeZRachunku(wysokoscRaty))
            {
                kredyt.SplacRate();
                wykonana = true;
            }
            else
            {
                wykonana = false;
            }
        }
    }
}
