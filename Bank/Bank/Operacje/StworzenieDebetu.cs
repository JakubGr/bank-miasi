﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    class StworzenieDebetu : Operacja
    {
        private IKontoDebetowe konto;
        private double wysokoscDebetu;

        internal StworzenieDebetu(IKontoDebetowe konto, string opis) : base(opis)
        {
            this.konto = konto;
            wysokoscDebetu = konto.WysokoscDebetu;
        }
    }
}
