﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class Wyplata : Operacja
    {
        private IKonto konto;
        private double kwota;

        public Wyplata(IKonto konto, double kwota, string opis) : base(opis)
        {
            this.kwota = kwota;
            this.konto = konto;
        }

        public override void WykonajOperacje()
        {
            DokonajWyplaty();
        }

        private void DokonajWyplaty()
        {
            dataRealizacji = DateTime.Now;
            if (konto.MozliwePobranie(kwota) && konto.PobierzPieniadzeZRachunku(kwota))
            {
                wykonana = true;
            }
            else
            {
                wykonana = false;
            }
        }
    }
}
