﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    class ZaciagniecieKredytu : Operacja
    {
        private IKonto kontoPowiazaneZKredytem;
        private IKredyt kredyt;
        public IKredyt Kredyt
        {
            get
            {
                return kredyt;
            }
        }
        private double wysokoscKredytu;

        internal ZaciagniecieKredytu(IKonto kontoPowiazaneZKredytem, double wysokoscKredytu, string opis) : base(opis)
        {
            this.kontoPowiazaneZKredytem = kontoPowiazaneZKredytem;
            this.wysokoscKredytu = wysokoscKredytu;
        }
        
        public override void WykonajOperacje()
        {
            ZaciagnijKredyt();
        }

        private void ZaciagnijKredyt()
        {
            dataRealizacji = DateTime.Now;
            kredyt = new Kredyt(12, new OdsetkiKredytu());
            if (kredyt == null)
            {
                wykonana = false;
            }
            else
            {
                wykonana = true;
                kontoPowiazaneZKredytem.DodajKredyt(kredyt);
            }
        }
    }
}
