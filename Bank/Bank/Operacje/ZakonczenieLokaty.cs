﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    class ZakonczenieLokaty : Operacja
    {
        private IKonto kontoPowiazaneZLokata;
        private ILokata usuwanaLokata;
        private bool zerwana;
        internal bool Zerwana
        {
            get
            {
                return zerwana;
            }
        }

        internal ZakonczenieLokaty(IKonto kontoPowiazaneZLokata, ILokata usuwanaLokata, bool zerwana, string opis) : base(opis)
        {
            this.kontoPowiazaneZLokata = kontoPowiazaneZLokata;
            this.usuwanaLokata = usuwanaLokata;
            this.zerwana = zerwana;
        }

        public override void WykonajOperacje()
        {
            ZakonczLokate();
        }

        private void ZakonczLokate()
        {
            dataRealizacji = DateTime.Now;
            double kwota = usuwanaLokata.ZakonczLokate();
            kontoPowiazaneZLokata.WplacPieniadzeNaRachunek(kwota);
            wykonana = true;
        }
    }
}
