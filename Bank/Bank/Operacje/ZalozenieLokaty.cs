﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class ZalozenieLokaty : Operacja
    {
        private IKonto kontoPowiazaneZLokata;
        private ILokata zakladanaLokata;
        public ILokata ZakladanaLokata
        {
            get
            {
                return zakladanaLokata;
            }
        }
        private double srodkiNaLokate;
        private DateTime terminRozpoczeciaLokaty;

        public ZalozenieLokaty(IKonto kontoPowiazaneZLokata, double srodkiNaLokate, DateTime terminRozpoczeciaLokaty, string opis) : base(opis)
        {
            this.kontoPowiazaneZLokata = kontoPowiazaneZLokata;
            this.srodkiNaLokate = srodkiNaLokate;
            this.terminRozpoczeciaLokaty = terminRozpoczeciaLokaty;
        }

        public override void WykonajOperacje()
        { 
            ZalozLokate();
        }

        private void ZalozLokate()
        {
            dataRealizacji = DateTime.Now;
            if (kontoPowiazaneZLokata.MozliwePobranie(srodkiNaLokate) && kontoPowiazaneZLokata.PobierzPieniadzeZRachunku(srodkiNaLokate))
            {
                zakladanaLokata = new Lokata(12, new OdsetkiLokaty());
                zakladanaLokata.UtworzLokate(srodkiNaLokate, terminRozpoczeciaLokaty);
                wykonana = true;
                kontoPowiazaneZLokata.DodajLokate(zakladanaLokata);
            }
            else
            {
                wykonana = false;
            }
        }
    }
}
