﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    class ZmianaMechanizmuOdsetkowego : Operacja
    {
        private IKonto konto;
        private IOdsetkiKonta noweOdsetki;

        internal ZmianaMechanizmuOdsetkowego(IKonto konto, IOdsetkiKonta noweOdsetki, string opis)
            : base(opis)
        {
            this.konto = konto;
            this.noweOdsetki = noweOdsetki;
        }
    }
}
