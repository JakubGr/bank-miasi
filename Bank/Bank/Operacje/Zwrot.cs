﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Operacje
{
    public class Zwrot : Operacja
    {

        private double kwota;
        private IKonto konto;

        internal Zwrot(IKonto konto, double kwota, string opis) : base(opis)
        {
            this.kwota = kwota;
            this.konto = konto;
        }

        public override void WykonajOperacje()
        {
            base.WykonajOperacje();
        }

        private void DokonajWplaty()
        {
            dataRealizacji = DateTime.Now;
            wykonana = konto.WplacPieniadzeNaRachunek(kwota);
        }

    }
}
