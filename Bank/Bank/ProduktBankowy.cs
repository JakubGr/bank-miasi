﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public abstract class ProduktBankowy : IProduktBankowy
    {
        protected List<Operacja> historiaOperacji;
        public List<Operacja> HistoriaOperacji
        {
            get
            {
                return historiaOperacji;
            }
        }

        protected int id;
        private static int nextId = 0;

        public ProduktBankowy()
        {
            this.id = nextId;
            nextId++;

            this.historiaOperacji = new List<Operacja>();
        }

        public virtual IProduktBankowy Akceptuj(IRaportSelekcja raport)
        {
            return null;
        }

        public virtual double Akceptuj(IRaportAgregacja raport)
        {
            return 0;
        }
    }
}
