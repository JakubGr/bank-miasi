﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Raporty
{
    public class Raport_AktywneKredyty : IRaportSelekcja
    {
        public IProduktBankowy Wybierz(IKonto konto)
        {
            return null;
        }

        public IProduktBankowy Wybierz(IKontoDebetowe kontoDebetowe)
        {
            return null;
        }

        public IProduktBankowy Wybierz(IKredyt kredyt)
        {
            if(kredyt.DataZakonczenia < DateTime.Now)
            {
                return kredyt;
            }
            return null;
        }

        public IProduktBankowy Wybierz(ILokata lokata)
        {
            return null;
        }
    }
}
