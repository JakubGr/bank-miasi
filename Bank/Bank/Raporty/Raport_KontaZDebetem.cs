﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Raporty
{
    public class Raport_KontaZDebetem : IRaportSelekcja
    {
        public IProduktBankowy Wybierz(ILokata lokata)
        {
            return null;
        }

        public IProduktBankowy Wybierz(IKontoDebetowe kontoDebetowe)
        {
            if (kontoDebetowe.Saldo < 0)
            {
                return kontoDebetowe;
            }
            return null;
        }

        public IProduktBankowy Wybierz(IKredyt kredyt)
        {
            return null;
        }

        public IProduktBankowy Wybierz(IKonto konto)
        {
            return null;
        }
    }
}
