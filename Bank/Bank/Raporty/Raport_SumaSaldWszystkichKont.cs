﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Raporty
{
    public class Raport_SumaSaldWszystkichKont : IRaportAgregacja
    {
        public double Wybierz(ILokata lokata)
        {
            return 0;
        }

        public double Wybierz(IKontoDebetowe kontoDebetowe)
        {
            return kontoDebetowe.Saldo;
        }

        public double Wybierz(IKredyt kredyt)
        {
            return 0;
        }

        public double Wybierz(IKonto konto)
        {
            return konto.Saldo;
        }
    }
}
