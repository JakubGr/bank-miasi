﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Raporty
{
    public class Raport_WysokoscAktywnychKredytow : IRaportAgregacja
    {
        public double Wybierz(IKonto konto)
        {
            return 0;
        }

        public double Wybierz(IKontoDebetowe kontoDebetowe)
        {
            return 0;
        }

        public double Wybierz(IKredyt kredyt)
        {
            if(kredyt.DataZakonczenia < DateTime.Now)
            {
                return kredyt.WysokoscKredytu;
            }
            return 0;
        }

        public double Wybierz(ILokata lokata)
        {
            return 0;
        }
    }
}
