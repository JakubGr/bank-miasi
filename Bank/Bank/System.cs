﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public class System
    {
        private static List<IBank> banki;

        public static void init()
        {
            banki = new List<IBank>();
        }

        public static void Main()
        {
           
        }

        public static void DodajBank(IBank bank)
        {
            banki.Add(bank);
        }

        public static IKonto ZwrocKonto(string nrRachunku)
        {
            foreach(IBank bank in banki)
            {
                IKonto konto = bank.ZwrocKonto(nrRachunku);
                if(konto != null)
                {
                    return konto;
                }
            }
            return null;
        }
    }
}
