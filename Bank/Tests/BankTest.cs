﻿using Bank;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Tests
{

    [TestFixture]
    class BankTest
    {


        Bank bank;
        Mock<IKlient> mockKlient;
        Mock<IKonto> mockKonto;
        Mock<IOdsetkiKonta> mockOdsetki;
        Mock<IOdsetkiKonta> mockNoweOdsetki;

        [SetUp]
        public void SetUp()
        {
          bank = new Bank();
          mockKlient = new Mock<IKlient>();
          mockKonto = new Mock<IKonto>();
          mockOdsetki = new Mock<IOdsetkiKonta>();
          mockNoweOdsetki = new Mock<IOdsetkiKonta>();

          bank.DodajKlienta(mockKlient.Object);
          bank.DodajProdukt(mockKonto.Object);
         
          mockKonto.Setup(konto => konto.ZmienMechanizmOdsetkowy(mockNoweOdsetki.Object, "")).Returns(true);
         
        }


        [Test]
        public void DodajKlientaTest_UzytkownikNieJestPusty()
        {
            bool wynik = bank.DodajKlienta(mockKlient.Object);
            Assert.IsTrue(wynik);
        }

        [Test]
        public void DodajKlientaTest_UzytkownikJestPusty()
        {
            bool wynik = bank.DodajKlienta(null);
            Assert.IsFalse(wynik);
        }

        [Test]
        public void UtworzKontoTest_KlientIstnieje()
        {
            IKonto ikonto = bank.UtworzKonto(mockKlient.Object);
            Assert.IsNotNull(ikonto);
        }

        [Test]
        public void UtworzKontoTest_KlientNieIstnieje()
        {
            IKonto ikonto = bank.UtworzKonto(null);
            Assert.IsNull(ikonto);
        }


        [Test]
        public void ZweryfikujKredytTest_ProduktIstnieje()
        {
            bool wynik = bank.ZweryfikujKredyt(mockKonto.Object);
            Assert.IsTrue(true);
        }


        [Test]
        public void ZmienMechanizmOdsetek()
        {
            bool wynik = bank.ZmienMechanizmOdsetek(mockKonto.Object, mockOdsetki.Object);
            Assert.IsTrue(wynik);
        }

    
        

       

    }
}
