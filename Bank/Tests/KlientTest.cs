﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Bank.Tests
{
    [TestFixture]
    public class KlientTest
    {
        Klient klient;
        Klient niebedacyKlientemBanku;
        Mock<IBank> mockBank = new Mock<IBank>();
        Mock<ILokata> mockLokata = new Mock<ILokata>();
        Mock<IKredyt> mockKredyt = new Mock<IKredyt>();
        Mock<IKonto> mockKonto = new Mock<IKonto>();
        List<IKlient> listaKlientow = new List<IKlient>();

        [SetUp]
        public void SetUp()
        {
            klient = new Klient("Jan", "Kowalski", "blablabla", "jan@kowalski.com", "abcdef", "123456789");
            niebedacyKlientemBanku = new Klient("Piotr", "Nowak", "blablabla", "piotr@nowak.com", "dvbsjd", "987654321");
            mockBank.Setup(bank => bank.UtworzKonto(niebedacyKlientemBanku)).Returns<IKonto>(null);
            mockBank.Setup(bank => bank.UtworzKonto(klient)).Returns(mockKonto.Object);

            mockKonto.SetupGet(konto => konto.NrRachunku).Returns("123456789");
            mockKonto.Setup(konto => konto.Przelew(It.IsAny<string>(), It.IsAny<double>(), It.IsAny<string>())).Returns(true);
            mockKonto.Setup(konto => konto.Wyplata(It.IsAny<double>(), It.IsAny<string>())).Returns(true);
            mockKonto.Setup(konto => konto.Wplata(It.IsAny<double>(), It.IsAny<string>())).Returns(true);
            mockKonto.Setup(konto => konto.ZalozLokate(It.IsAny<double>(), It.IsAny<DateTime>(), It.IsAny<string>())).Returns(true);
            mockKonto.Setup(konto => konto.ZerwijLokate(It.IsAny<ILokata>(), It.IsAny<string>())).Returns(true);
            mockKonto.Setup(konto => konto.ZaciagnijKredyt(It.IsAny<double>(), It.IsAny<string>())).Returns(true);
            mockKonto.Setup(konto => konto.SplacRateKredytu(It.IsAny<IKredyt>(), It.IsAny<string>())).Returns(true);
            klient.UtworzKonto(mockBank.Object);
        }

        [Test]
        public void UtworzKontoTest_UzytkownikNieJestKlientemBanku()
        {
            bool wynik = niebedacyKlientemBanku.UtworzKonto(mockBank.Object);
            Assert.IsFalse(wynik);
        }

        [Test]
        public void UtworzKontoTest_UzytkownikJestKlientemBanku()
        {
            bool wynik = klient.UtworzKonto(mockBank.Object);
            Assert.IsTrue(wynik);
        }

        [Test]
        public void WykonajPrzelewTest_ZlyNumerRachunkuNadawcy()
        {
            bool wynik = klient.WykonajPrzelew("321456789", "789456123", 100.00, "");
            Assert.IsFalse(wynik);
        }

        [Test]
        public void WykonajPrzelewTest_DobryNumerRachunkuNadawcy()
        {
            bool wynik = klient.WykonajPrzelew("123456789", "789456123", 100.00, "");
            Assert.IsTrue(wynik);
        }

        [Test]
        public void PobierzPieniadzeTest_ZlyNumerRacunku()
        {
            bool wynik = klient.PobierzPieniadze("987123456", 100.00, "");
            Assert.IsFalse(wynik);
        }

        [Test]
        public void PobierzPieniadzeTest_DobryNumerRacunku()
        {
            bool wynik = klient.PobierzPieniadze("123456789", 100.00, "");
            Assert.IsTrue(wynik);
        }

        [Test]
        public void WplacPieniadzeTest_ZlyNumerRacunku()
        {
            bool wynik = klient.WplacPieniadze("987123456", 100.00, "");
            Assert.IsFalse(wynik);
        }

        [Test]
        public void WplacPieniadzeTest_DobryNumerRacunku()
        {
            bool wynik = klient.WplacPieniadze("123456789", 100.00, "");
            Assert.IsTrue(wynik);
        }

        [Test]
        public void ZalozLokateTest_ZlyNumerRacunku()
        {
            bool wynik = klient.ZalozLokate("987123456", 10000.00, DateTime.Today, "");
            Assert.IsFalse(wynik);
        }

        [Test]
        public void ZalozLokateTest_DobryNumerRacunku()
        {
            bool wynik = klient.ZalozLokate("123456789", 10000.00, DateTime.Today, "");
            Assert.IsTrue(wynik);
        }

        [Test]
        public void ZerwijLokateTest_ZlyNumerRacunku()
        {
            bool wynik = klient.ZerwijLokate("987123456", mockLokata.Object, "");
            Assert.IsFalse(wynik);
        }

        [Test]
        public void ZerwijLokateTest_DobryNumerRacunku()
        {
            bool wynik = klient.ZerwijLokate("123456789", mockLokata.Object, "");
            Assert.IsTrue(wynik);
        }

        [Test]
        public void WezKredytTest_ZlyNumerRacunku()
        {
            bool wynik = klient.WezKredyt("987123456", 100000.00, "");
            Assert.IsFalse(wynik);
        }

        [Test]
        public void WezKredytTest_DobryNumerRacunku()
        {
            bool wynik = klient.WezKredyt("123456789", 100000.00, "");
            Assert.IsTrue(wynik);
        }

        [Test]
        public void SplacRateKredytuTest_ZlyNumerRacunku()
        {
            bool wynik = klient.SplacRateKredytu("987123456", mockKredyt.Object, "");
            Assert.IsFalse(wynik);
        }

        [Test]
        public void SplacRateKredytuTest_DobryNumerRacunku()
        {
            bool wynik = klient.SplacRateKredytu("123456789", mockKredyt.Object, "");
            Assert.IsTrue(wynik);
        }
    }
}
