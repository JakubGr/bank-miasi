﻿using Bank;
using Moq;
using NUnit.Framework;

namespace Bank.Tests
{
    [TestFixture]
    public class KontoDebetoweTest
    {
        KontoDebetowe kontoDebetowe;
        Mock<IKonto> mockKonto;

        double maxDebet;
        double srodkiNaKoncie;

        [SetUp]
        public void SetUp()
        {
            maxDebet = 500.00;
            srodkiNaKoncie = 500.00;

            mockKonto = new Mock<IKonto>();

            mockKonto.Setup(konto => konto.MozliwePobranie(It.IsAny<double>())).Returns(false);
            mockKonto.Setup(konto => konto.MozliwePobranie(It.IsInRange(0.00, srodkiNaKoncie, Range.Inclusive))).Returns(true);

            mockKonto.Setup(konto => konto.PobierzPieniadzeZRachunku(It.IsAny<double>())).Returns(false);
            mockKonto.Setup(konto => konto.PobierzPieniadzeZRachunku(It.IsInRange(0.0, srodkiNaKoncie, Range.Inclusive)))
                .Callback<double>(kwota => srodkiNaKoncie -= kwota)
                .Returns(true);

            mockKonto.Setup(konto => konto.WplacPieniadzeNaRachunek(It.IsAny<double>())).Returns(false);
            mockKonto.Setup(konto => konto.WplacPieniadzeNaRachunek(It.IsInRange(0, double.MaxValue, Range.Inclusive)))
                .Callback<double>(kwota => srodkiNaKoncie += kwota)
                .Returns(true);

            mockKonto.SetupGet(konto => konto.Saldo).Returns(() => { return srodkiNaKoncie; });

            kontoDebetowe = new KontoDebetowe(mockKonto.Object, maxDebet);
        }

        [Test]
        public void MozliwePobranieTest_KwotaZaWysoka()
        {
            bool wynik = kontoDebetowe.MozliwePobranie(maxDebet + srodkiNaKoncie + 100.00);
            Assert.IsFalse(wynik);
        }

        [Test]
        public void MozliwePobranieTest_UjemnaKwota()
        {
            bool wynik = kontoDebetowe.MozliwePobranie(-500.00);
            Assert.IsFalse(wynik);
        }

        [Test]
        public void MozliwePobranieTest_UtorzenieDebetu()
        {
            bool wynik = kontoDebetowe.MozliwePobranie(srodkiNaKoncie + 100.00);
            Assert.IsTrue(wynik);
        }

        [Test]
        public void MozliwePobranieTest_BezTworzeniaDebetu()
        {
            bool wynik = kontoDebetowe.MozliwePobranie(srodkiNaKoncie - 100.00);
            Assert.IsTrue(wynik);
        }

        [Test]
        public void MozliwePobranieTest_GornyLimitDebetu()
        {
            bool wynik = kontoDebetowe.MozliwePobranie(maxDebet + srodkiNaKoncie);
            Assert.IsTrue(wynik);
        }

        [Test]
        public void PobierzPieniadzeZRachunkuTest_ZaWysokaKwota()
        {
            bool wynik = kontoDebetowe.PobierzPieniadzeZRachunku(maxDebet + srodkiNaKoncie + 100.00);
            Assert.IsFalse(wynik);
            Assert.AreEqual(500.00, kontoDebetowe.Saldo);
        }

        [Test]
        public void PobierzPieniadzeZRachunkuTest_UjemnaKwota()
        {
            bool wynik = kontoDebetowe.PobierzPieniadzeZRachunku(-500.00);
            Assert.IsFalse(wynik);
            Assert.AreEqual(500.00, kontoDebetowe.Saldo);
        }

        [Test]
        public void PobierzPieniadzeZRachunkuTest_BezTworzeniaDebetu()
        {
            bool wynik = kontoDebetowe.PobierzPieniadzeZRachunku(srodkiNaKoncie - 100.00);
            Assert.IsTrue(wynik);
            Assert.AreEqual(100.00, kontoDebetowe.Saldo);
        }

        [Test]
        public void PobierzPieniadzeZRachunkuTest_UtworzenieDebetu()
        {
            bool wynik = kontoDebetowe.PobierzPieniadzeZRachunku(srodkiNaKoncie + 100.00);
            Assert.IsTrue(wynik);
            Assert.AreEqual(-100.00, kontoDebetowe.Saldo);
        }

        [Test]
        public void PobierzPieniadzeZRachunkuTest_GornyLimitDebetu()
        {
            bool wynik = kontoDebetowe.PobierzPieniadzeZRachunku(maxDebet + srodkiNaKoncie);
            Assert.IsTrue(wynik);
            Assert.AreEqual(-maxDebet, kontoDebetowe.Saldo);
        }

        [Test]
        public void WplacPieniadzeNaRachunekTest_UjemnaKwota()
        {
            bool wynik = kontoDebetowe.WplacPieniadzeNaRachunek(-500.00);
            Assert.IsFalse(wynik);
            Assert.AreEqual(500.00, kontoDebetowe.Saldo);
        }

        [Test]
        public void WplacPieniadzeNaRachunekTest_WplataWiekszaNizDebet()
        {
            kontoDebetowe.PobierzPieniadzeZRachunku(srodkiNaKoncie + 100.00);
            bool wynik = kontoDebetowe.WplacPieniadzeNaRachunek(200.00);
            Assert.IsTrue(wynik);
            Assert.AreEqual(100.00, kontoDebetowe.Saldo);
        }

        [Test]
        public void WplacPieniadzeNaRachunekTest_WplataRownaDebetowi()
        {
            kontoDebetowe.PobierzPieniadzeZRachunku(srodkiNaKoncie + 100.00);
            bool wynik = kontoDebetowe.WplacPieniadzeNaRachunek(100.00);
            Assert.IsTrue(wynik);
            Assert.AreEqual(0.00, kontoDebetowe.Saldo);
        }

        [Test]
        public void WplacPieniadzeNaRachunekTest_WplataMniejszaNizDebet()
        {
            kontoDebetowe.PobierzPieniadzeZRachunku(srodkiNaKoncie + 100.00);
            bool wynik = kontoDebetowe.WplacPieniadzeNaRachunek(50.00);
            Assert.IsTrue(wynik);
            Assert.AreEqual(-50.00, kontoDebetowe.Saldo);
        }
    }
}
