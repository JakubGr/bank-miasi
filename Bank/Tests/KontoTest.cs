﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Tests
{
    [TestFixture]
    class KontoTest
    {
        Double kwotaZaWysoka = 1000;
        Double kwotaZaWysokaDlaDebetu = 1500;
        Double kwotaOk = 500;
        Double kwotaUjemna = -100;

        Konto kontodoPrzelewu;
        Konto konto;

        Mock<IKlient> mockKlient;
        Mock<IBank> mockBank;
        Mock<IOdsetkiKonta> mockOdsetki;
        Mock<IKredyt> mockKredyt;
        Mock<ILokata> mockLokata;
       
   
        
        [SetUp]
        public void SetUp()
        {
            mockKlient = new Mock<IKlient>();
            mockOdsetki = new Mock<IOdsetkiKonta>();
            mockBank = new Mock<IBank>();
            mockKredyt = new Mock<IKredyt>();
            mockLokata = new Mock<ILokata>();            

            konto = new Konto(mockKlient.Object, mockBank.Object, mockOdsetki.Object);
            kontodoPrzelewu = new Konto(mockKlient.Object, mockBank.Object, mockOdsetki.Object);
            kontodoPrzelewu.WplacPieniadzeNaRachunek(1000.0);
            mockBank.Setup(bank => bank.ZwrocKonto("987654321")).Returns(konto);
            System.init();
            System.DodajBank(mockBank.Object);

            konto.WplacPieniadzeNaRachunek(800);            

        }

        [Test]
        public void MozliwePobranieTest_KwotaWiekszaOdSaldo()
        {
            bool wynik = konto.MozliwePobranie(kwotaZaWysoka);
            Assert.False(wynik);
        }

        [Test]
        public void MozliwePobranieTest_KwotaUjemna()
        {
            bool wynik = konto.MozliwePobranie(kwotaUjemna);
            Assert.False(wynik);
        }

        [Test]
        public void MozliwePobranieTest_PobranieMozliwe()
        {
            bool wynik = konto.MozliwePobranie(kwotaOk);
            Assert.True(wynik);
        }
        
        public void PobierzPieniadzeZRachunkuTest_PobranieMozliwe()
        {
            bool wynik = konto.PobierzPieniadzeZRachunku(kwotaOk);
            Assert.True(wynik);
        }

        public void PobierzPieniadzeZRachunkuTest_PobranieNiemozliwe()
        {
            bool wynik = konto.PobierzPieniadzeZRachunku(kwotaZaWysokaDlaDebetu);
            Assert.False(wynik);
        }

        [Test]
        public void WplacPieniadzeNaRachunekTest_KwotaUjemna()
        {
            bool wynik = konto.WplacPieniadzeNaRachunek(kwotaUjemna);
            Assert.False(wynik);
        }

        [Test]
        public void PrzelewTest_DobryOdbiorca()
        {

            bool wynik = kontodoPrzelewu.Przelew("987654321", kwotaOk, "");
            Assert.True(wynik);
        }

        [Test]
        public void WyplataTest_DokonanieWyplaty()
        {
            bool wynik = konto.Wyplata(kwotaOk, "");
            Assert.True(wynik);
        }

        [Test]
        public void WplataTest_DokonanieWplaty()
        {
            bool wynik = konto.Wplata(kwotaOk, "");
            Assert.True(wynik);
        }

        [Test]
        public void ZwrotTest_DokonanieZwrotu()
        {
            bool wynik = konto.Zwrot(kwotaOk, "");
            Assert.True(wynik);
        }

        [Test]
        public void NaliczOdsetkiTest_DokonanieNaliczania()
        {
            bool wynik = konto.NaliczOdsetki("");
            Assert.True(wynik);
        }

        [Test]
        public void ZmienMechanizmOdsetkowy()
        {
            bool wynik = konto.ZmienMechanizmOdsetkowy(mockOdsetki.Object, "");
            Assert.True(wynik);
        }

        [Test]
        public void ZalozLokateTest()
        {
            bool wynik = konto.ZalozLokate(kwotaOk, DateTime.Now, "");
            Assert.True(wynik);
        }

        [Test]
        public void ZerwijLokateTest_LokataNieIstnieje()
        {
            bool wynik = konto.ZerwijLokate(mockLokata.Object, "");
            Assert.False(wynik);
        }

        [Test]
        public void ZaciagnijKredytTest_KredytNieIstnieje()
        {
            bool wynik = konto.ZaciagnijKredyt(kwotaOk, "");
            Assert.True(wynik);
        }

        [Test]
        public void SplacRateKredytuTest()
        {
            bool wynik = konto.SplacRateKredytu(mockKredyt.Object, "");
            Assert.True(wynik);
        }

    }
}
