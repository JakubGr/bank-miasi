﻿using Moq;
using NUnit.Framework;
using System;

namespace Bank.Tests
{
    [TestFixture]
    public class KredytTest
    {

        Kredyt kredyt;
        Mock<IOdsetkiKredytu> mockOdsetki;
        Mock<IKonto> mockKonto;
        Mock<IKonto> mockKontoBezKredytu;

        [SetUp]
        public void SetUp()
        {
            mockOdsetki = new Mock<IOdsetkiKredytu>();
            mockKonto = new Mock<IKonto>();
            mockKontoBezKredytu = new Mock<IKonto>();

            mockKonto.Setup(konto => konto.ZweryfikujKredyt()).Returns(true);
            mockKontoBezKredytu.Setup(konto => konto.ZweryfikujKredyt()).Returns(false);

            kredyt = new Kredyt(12, mockOdsetki.Object);
        }

        [Test]
        public void WezKredytTest_NegatywnaWeryfikacja()
        {
            bool wynik = kredyt.WezKredyt(100000.00, mockKontoBezKredytu.Object);
            Assert.IsFalse(wynik);
        }

        [Test]
        public void WezKredytTest_PozytywnaWeryfikacja()
        {
            bool wynik = kredyt.WezKredyt(100000.00, mockKonto.Object);
            Assert.IsTrue(wynik);
        }
    }
}
