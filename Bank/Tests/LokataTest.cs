﻿using Moq;
using NUnit.Framework;
using System;

namespace Bank.Tests
{
    [TestFixture]
    public class LokataTest
    {
        Lokata lokata;
        Mock<IOdsetkiLokaty> mockOdsetki;

        [SetUp]
        public void SetUp()
        {
            mockOdsetki = new Mock<IOdsetkiLokaty>();

            mockOdsetki.Setup(odsetki => odsetki.ObliczOdsetki(It.IsAny<Lokata>())).Returns<Lokata>(l => l.WysokoscLokaty * 0.01);

            lokata = new Lokata(6, mockOdsetki.Object);
        }

        [Test]
        public void ZakonczLokate_PrzedTerminem()
        {
            DateTime dzis = DateTime.Today;
            DateTime startLokaty = dzis.AddMonths(-3);
            lokata.UtworzLokate(10000.00, startLokaty);
            double zwrot = lokata.ZakonczLokate();
            Assert.AreEqual(10000.00, zwrot);
        }

        [Test]
        public void ZakonczLokate_ZgodnieZTerminem()
        {
            DateTime dzis = DateTime.Today;
            DateTime startLokaty = dzis.AddMonths(-6);
            lokata.UtworzLokate(10000.00, startLokaty);
            double zwrot = lokata.ZakonczLokate();
            Assert.AreEqual(1.01 * 10000.00, zwrot);
        }
    }
}
