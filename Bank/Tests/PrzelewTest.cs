﻿using Bank;
using Moq;
using NUnit.Framework;

namespace Bank.Tests
{
    [TestFixture]
    public class PrzelewTest
    {
        Przelew przelew_ZaMalaIloscSrodkowNaKoncieNadawcy;
        Przelew przelew_ZaMalaIloscSrodkowNaKoncieNadawcyPoSprawdzeniuIchDostepnosci;
        Przelew przelew_OdpowiedniaIloscSrodkowNaKoncieNadawcy;
        Mock<IKonto> mockKontoNadawcy;
        Mock<IKonto> mockKontoOdbiorcy;

        [SetUp]
        public void SetUp()
        {
            mockKontoNadawcy = new Mock<IKonto>();
            mockKontoOdbiorcy = new Mock<IKonto>();

            mockKontoNadawcy.Setup(konto => konto.MozliwePobranie(It.IsAny<double>())).Returns(false);
            mockKontoNadawcy.Setup(konto => konto.MozliwePobranie(It.IsInRange(0.0, 2000.00, Range.Inclusive))).Returns(true);

            mockKontoNadawcy.Setup(konto => konto.PobierzPieniadzeZRachunku(It.IsAny<double>())).Returns(false);
            mockKontoNadawcy.Setup(konto => konto.PobierzPieniadzeZRachunku(It.IsInRange(0.0, 1800.00, Range.Inclusive))).Returns(true);

            mockKontoOdbiorcy.Setup(konto => konto.WplacPieniadzeNaRachunek(It.IsAny<double>())).Returns(true);
            mockKontoOdbiorcy.Setup(konto => konto.WplacPieniadzeNaRachunek(It.Is<double>(kwota => kwota < 0.0))).Returns(false);

            przelew_ZaMalaIloscSrodkowNaKoncieNadawcy = new Przelew(mockKontoNadawcy.Object, mockKontoOdbiorcy.Object, 3000.00, "");
            przelew_ZaMalaIloscSrodkowNaKoncieNadawcyPoSprawdzeniuIchDostepnosci = new Przelew(mockKontoNadawcy.Object, mockKontoOdbiorcy.Object, 1900.00, "");
            przelew_OdpowiedniaIloscSrodkowNaKoncieNadawcy = new Przelew(mockKontoNadawcy.Object, mockKontoOdbiorcy.Object, 1000.00, "");
        }

        [Test]
        public void WykonajPrzelew_ZaMalaIloscSrodkowNaKoncieNadawcy()
        {
            przelew_ZaMalaIloscSrodkowNaKoncieNadawcy.WykonajOperacje();
            Assert.IsFalse((bool)przelew_ZaMalaIloscSrodkowNaKoncieNadawcy.Wykonana);
        }

        [Test]
        public void WykonajPrzelew_ZaMalaIloscSrodkowNaKoncieNadawcyPoSprawdzeniuIchDostepnosci()
        {
            przelew_ZaMalaIloscSrodkowNaKoncieNadawcyPoSprawdzeniuIchDostepnosci.WykonajOperacje();
            Assert.IsFalse((bool)przelew_ZaMalaIloscSrodkowNaKoncieNadawcyPoSprawdzeniuIchDostepnosci.Wykonana);
        }

        [Test]
        public void WykonajPrzelew_OdpowiedniaIloscSrodkowNaKoncieNadawcy()
        {
            przelew_OdpowiedniaIloscSrodkowNaKoncieNadawcy.WykonajOperacje();
            Assert.IsTrue((bool)przelew_OdpowiedniaIloscSrodkowNaKoncieNadawcy.Wykonana);
        }
    }
}
