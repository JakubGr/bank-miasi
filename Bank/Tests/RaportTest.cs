﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using Moq;

namespace Bank.Tests
{
    [TestFixture]
    public class RaportTest
    {

        List<IProduktBankowy> produkty;
        List<IProduktBankowy> kontaDebetowe;
        List<IProduktBankowy> niesplaconeKredyty;

        Mock<IKonto> mockKonto1;
        Mock<IKonto> mockKonto2;
        Mock<IKontoDebetowe> mockKontoDebetowe1;
        Mock<IKontoDebetowe> mockKontoDebetowe2;

        Mock<IKredyt> mockKredyt;
        Mock<IKredyt> mockKredytNiesplacony1;
        Mock<IKredyt> mockKredytNiesplacony2;

        DateTime dzis;
  
        [SetUp]
        public void SetUp()
        {
            dzis = DateTime.Today;
           
            produkty = new List<IProduktBankowy>();
            kontaDebetowe = new List<IProduktBankowy>();
            niesplaconeKredyty = new List<IProduktBankowy>();

            mockKonto1 = new Mock<IKonto>();
            mockKonto2 = new Mock<IKonto>();            
            mockKontoDebetowe1 = new Mock<IKontoDebetowe>();
            mockKontoDebetowe2 = new Mock<IKontoDebetowe>();
            mockKredyt = new Mock<IKredyt>();
            mockKredytNiesplacony1 = new Mock<IKredyt>();
            mockKredytNiesplacony2 = new Mock<IKredyt>();

            mockKonto1.SetupGet(konto => konto.Saldo).Returns(1000.0);
            mockKonto2.SetupGet(konto => konto.Saldo).Returns(2000.0);
            mockKontoDebetowe1.SetupGet(konto => konto.Saldo).Returns(1000.0);
            mockKontoDebetowe2.SetupGet(konto => konto.Saldo).Returns(2000.0);

            mockKredyt.SetupGet(kredyt => kredyt.DataZakonczenia).Returns(dzis.AddMonths(-3));
            mockKredytNiesplacony1.SetupGet(kredyt => kredyt.DataZakonczenia).Returns(dzis.AddMonths(3));
            mockKredytNiesplacony2.SetupGet(kredyt => kredyt.DataZakonczenia).Returns(dzis.AddMonths(6));

            produkty.Add(mockKonto1.Object);
            produkty.Add(mockKonto2.Object);
            produkty.Add(mockKontoDebetowe1.Object);
            produkty.Add(mockKontoDebetowe2.Object);
            produkty.Add(mockKredyt.Object);
            produkty.Add(mockKredytNiesplacony1.Object);
            produkty.Add(mockKredytNiesplacony2.Object);

            kontaDebetowe.Add(mockKontoDebetowe1.Object);
            kontaDebetowe.Add(mockKontoDebetowe2.Object);

            niesplaconeKredyty.Add(mockKredytNiesplacony1.Object);
            niesplaconeKredyty.Add(mockKredytNiesplacony2.Object);


        }

        [Test]
        public void PobierzSaldoOgolneTest()
        {
            //double wynik = Raport.PobierzSaldoOgolne(produkty);
            //Assert.AreEqual(6000.0, wynik);
        }

        [Test]
        public void PobierzKontaZMozliwymDebetemTest()
        {
            //List<IKonto> wynikRaportu = Raport.PobierzKontaZMozliwymDebetem(produkty);
            //Assert.AreEqual(kontaDebetowe,wynikRaportu);
        }


        [Test]
        public void PobierzNiespłaconeKredyty()
        {
            //List<IKredyt> wynikRaportu = Raport.PobierzNiespłaconeKredyty(produkty, dzis);
            //Assert.AreEqual(niesplaconeKredyty, wynikRaportu);
        }

    }
}

