﻿using Bank;
using Moq;
using NUnit.Framework;

namespace Bank.Tests
{
    [TestFixture]
    public class SplataRatyKredytuTest
    {
        SplataRatyKredytu splataRatyKredytu_ZaWysokaRata;
        SplataRatyKredytu splataRatyKredytu_ZaWysokaRataPoSprawdzeniuDostepnosciSrodkow;
        SplataRatyKredytu splataRatyKredytu_OdpowiedniaRata;

        Mock<IKonto> mockKonto;
        Mock<IKredyt> mockKredyt_ZaWysokaRata;
        Mock<IKredyt> mockKredyt_ZaWysokaRataPoSprawdzeniuDostepnosciSrodkow;
        Mock<IKredyt> mockKredyt_OdpowiedniaRata;

        [SetUp]
        public void SetUp()
        {
            mockKonto = new Mock<IKonto>();
            mockKredyt_OdpowiedniaRata = new Mock<IKredyt>();
            mockKredyt_ZaWysokaRataPoSprawdzeniuDostepnosciSrodkow = new Mock<IKredyt>();
            mockKredyt_ZaWysokaRata = new Mock<IKredyt>();

            mockKonto.Setup(konto => konto.MozliwePobranie(It.IsAny<double>())).Returns(false);
            mockKonto.Setup(konto => konto.MozliwePobranie(It.IsInRange(0.00, 500.00, Range.Inclusive))).Returns(true);

            mockKonto.Setup(konto => konto.PobierzPieniadzeZRachunku(It.IsAny<double>())).Returns(false);
            mockKonto.Setup(konto => konto.PobierzPieniadzeZRachunku(It.IsInRange(0.00, 300.00, Range.Inclusive))).Returns(true);

            mockKredyt_ZaWysokaRata.SetupGet(kredyt => kredyt.RataKredytu).Returns(600.00);
            mockKredyt_ZaWysokaRataPoSprawdzeniuDostepnosciSrodkow.SetupGet(kredyt => kredyt.RataKredytu).Returns(400.00);
            mockKredyt_OdpowiedniaRata.SetupGet(kredyt => kredyt.RataKredytu).Returns(200.00);

            splataRatyKredytu_ZaWysokaRata = new SplataRatyKredytu(mockKonto.Object, mockKredyt_ZaWysokaRata.Object, "");
            splataRatyKredytu_ZaWysokaRataPoSprawdzeniuDostepnosciSrodkow = new SplataRatyKredytu(mockKonto.Object, mockKredyt_ZaWysokaRataPoSprawdzeniuDostepnosciSrodkow.Object, "");
            splataRatyKredytu_OdpowiedniaRata = new SplataRatyKredytu(mockKonto.Object, mockKredyt_OdpowiedniaRata.Object, "");
        }

        [Test]
        public void Splacrate_ZaWysokaRata()
        {
            splataRatyKredytu_ZaWysokaRata.WykonajOperacje();
            Assert.IsFalse((bool)splataRatyKredytu_ZaWysokaRata.Wykonana);
        }

        [Test]
        public void Splacrate_ZaWysokaRataPoSprawdzeniuDostepnosciSrodkow()
        {
            splataRatyKredytu_ZaWysokaRataPoSprawdzeniuDostepnosciSrodkow.WykonajOperacje();
            Assert.IsFalse((bool)splataRatyKredytu_ZaWysokaRataPoSprawdzeniuDostepnosciSrodkow.Wykonana);
        }

        [Test]
        public void Splacrate_OdpowiedniaRata()
        {
            splataRatyKredytu_OdpowiedniaRata.WykonajOperacje();
            Assert.IsTrue((bool)splataRatyKredytu_OdpowiedniaRata.Wykonana);
        }
    }
}
