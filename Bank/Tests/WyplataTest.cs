﻿using Bank;
using Moq;
using NUnit.Framework;

namespace Bank.Tests
{
    [TestFixture]
    public class WyplataTest
    {
        Wyplata wyplata_ZaMalaIloscSrodkow;
        Wyplata wyplata_ZaMalaIloscSrodkowPoSprawdzeniuIchDostepnosci;
        Wyplata wyplata_OdpowiedniaIloscSrodkow;

        Mock<IKonto> mockKonto;

        [SetUp]
        public void SetUp()
        {
            mockKonto = new Mock<IKonto>();

            mockKonto.Setup(konto => konto.MozliwePobranie(It.IsAny<double>())).Returns(false);
            mockKonto.Setup(konto => konto.MozliwePobranie(It.IsInRange(0.00, 1000.00, Range.Inclusive))).Returns(true);

            mockKonto.Setup(konto => konto.PobierzPieniadzeZRachunku(It.IsAny<double>())).Returns(false);
            mockKonto.Setup(konto => konto.PobierzPieniadzeZRachunku(It.IsInRange(0.00, 800.00, Range.Inclusive))).Returns(true);

            wyplata_ZaMalaIloscSrodkow = new Wyplata(mockKonto.Object, 1100.00, "");
            wyplata_ZaMalaIloscSrodkowPoSprawdzeniuIchDostepnosci = new Wyplata(mockKonto.Object, 900.00, "");
            wyplata_OdpowiedniaIloscSrodkow = new Wyplata(mockKonto.Object, 500.00, "");
        }

        [Test]
        public void DokonajWyplaty_ZaMalaIloscSrodkow()
        {
            wyplata_ZaMalaIloscSrodkow.WykonajOperacje();
            Assert.IsFalse((bool)wyplata_ZaMalaIloscSrodkow.Wykonana);
        }

        [Test]
        public void DokonajWyplaty_ZaMalaIloscSrodkowPoSprawdzeniuIchDostepnosci()
        {
            wyplata_ZaMalaIloscSrodkowPoSprawdzeniuIchDostepnosci.WykonajOperacje();
            Assert.IsFalse((bool)wyplata_ZaMalaIloscSrodkowPoSprawdzeniuIchDostepnosci.Wykonana);
        }

        [Test]
        public void DokonajWyplaty_OdpowiedniaIloscSrodkow()
        {
            wyplata_OdpowiedniaIloscSrodkow.WykonajOperacje();
            Assert.IsTrue((bool)wyplata_OdpowiedniaIloscSrodkow.Wykonana);
        }
    }
}
