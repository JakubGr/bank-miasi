﻿using Bank;
using Moq;
using NUnit.Framework;
using System;

namespace Bank.Tests
{
    [TestFixture]
    public class ZalozenieLokatyTest
    {
        ZalozenieLokaty zalozenieLokaty_KontoZOdpowiedniaIlosciaSrodkow;
        ZalozenieLokaty zalozenieLokaty_KontoZZaMalaIlosciaSrodkowPoSprawdzeniuIchDostepnosci;
        ZalozenieLokaty zalozenieLokaty_KontoZZaMalaIlosciaSrodkow;

        Mock<IKonto> mockKonto;

        [SetUp]
        public void SetUp()
        {
            mockKonto = new Mock<IKonto>();

            mockKonto.Setup(konto => konto.MozliwePobranie(It.IsAny<double>())).Returns(false);
            mockKonto.Setup(konto => konto.MozliwePobranie(It.IsInRange(0.00, 11500.00, Range.Inclusive))).Returns(true);

            mockKonto.Setup(konto => konto.PobierzPieniadzeZRachunku(It.IsAny<double>())).Returns(false);
            mockKonto.Setup(konto => konto.PobierzPieniadzeZRachunku(It.IsInRange(0.00, 10500.00, Range.Inclusive))).Returns(true);

            zalozenieLokaty_KontoZOdpowiedniaIlosciaSrodkow = new ZalozenieLokaty(mockKonto.Object, 10000.00, DateTime.Now, "");
            zalozenieLokaty_KontoZZaMalaIlosciaSrodkowPoSprawdzeniuIchDostepnosci = new ZalozenieLokaty(mockKonto.Object, 11000.00, DateTime.Now, "");
            zalozenieLokaty_KontoZZaMalaIlosciaSrodkow = new ZalozenieLokaty(mockKonto.Object, 12000.00, DateTime.Now, "");

        }

        [Test]
        public void ZalozLokate_NiewystarczajacaIloscSrodkowNaKoncie()
        {
            zalozenieLokaty_KontoZZaMalaIlosciaSrodkow.WykonajOperacje();
            Assert.IsFalse((bool)zalozenieLokaty_KontoZZaMalaIlosciaSrodkow.Wykonana);
        }

        [Test]
        public void ZalozLokate_NiewystarczajacaIloscSrodkowNaKonciePoSprawdzeniuIchDostepnosci()
        {
            zalozenieLokaty_KontoZZaMalaIlosciaSrodkowPoSprawdzeniuIchDostepnosci.WykonajOperacje();
            Assert.IsFalse((bool)zalozenieLokaty_KontoZZaMalaIlosciaSrodkowPoSprawdzeniuIchDostepnosci.Wykonana);
        }

        [Test]
        public void ZalozLokate_WystarczajacaIloscSrodkowNaKoncie()
        {
            zalozenieLokaty_KontoZOdpowiedniaIlosciaSrodkow.WykonajOperacje();
            Assert.IsTrue((bool)zalozenieLokaty_KontoZOdpowiedniaIlosciaSrodkow.Wykonana);
        }
    }
}
